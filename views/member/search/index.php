<!DOCTYPE html>
<?php
    $current_page = 'Search'; 
    $timestamp = date("YmdHis");
    $site_path = 'http://localhost/racf_dashboard/assets';
    $site_url = 'http://localhost/racf_dashboard/views';
    // $site_path = 'https://www.creamyw.com/dev/rent/assets';
    // $site_url = 'https://www.creamyw.com/dev/rent/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../../components/links.php'; ?>
</head>

<body>
    <div class="wrapper">
        <?php include '../../../components/side-menu-member.php'; ?>
        <div class="content__page search__page">
            <div class="wrapper">
                <div class="container-fluid">
                    <?php include '../../../components/header-member.php' ?>
                    <div class="py-xl-50 py-lg-40 py-md-30 py-20">
                        <div class="heading mb-lg-35 mb-20">
                            <h1 class="fw-bold text-primary"><?php echo $current_page ?></h1>
                        </div>
                        <form action="">
                            <div class="form card p-lg-25 p-15 mt-md-30 mt-20">
                                <div class="pinboard__search">
                                    <form action="">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="position-relative">
                                                    <input type="search"
                                                        placeholder="Find Cyber Friends from your pinboard" name=""
                                                        class="form-control" id="">
                                                    <button type="submit" class="btn__custom">
                                                        <img src="<?php echo $site_path ?>/images/icons/search.svg"
                                                            alt="">
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-12 mt-10">
                                                <div class="d-flex align-items-center flex-wrap">
                                                    <div
                                                        class="filter__group d-flex align-items-center flex-wrap me-md-10 me-0 mb-md-0 mb-10">
                                                        <span class="me-10">Gender:</span>
                                                        <div class="actions">
                                                            <button class="btn__custom btn__action active">
                                                                Both
                                                            </button>
                                                            <button class="btn__custom btn__action">
                                                                Male
                                                            </button>
                                                            <button class="btn__custom btn__action">
                                                                Female
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="filter__group d-flex align-items-center flex-wrap">
                                                        <span class="me-10">Status:</span>
                                                        <div class="actions">
                                                            <button class="btn__custom btn__action active">
                                                                Both
                                                            </button>
                                                            <button class="btn__custom btn__action">
                                                                Online
                                                            </button>
                                                            <button class="btn__custom btn__action">
                                                                Offline
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="cards__profile mt-md-60 mt-50">
                                        <div class="row">
                                            <?php
                                                for ($i=0; $i < 9; $i++) { 
                                                    echo'
                                                    <div class="col-md-4">
                                                        <a href="'.$site_url.'/member/cyberprofile" class="d-block card__item position-relative">
                                                            <!-- online, offline -->
                                                            '.(($i === 1) || ($i === 3) || ($i === 5) 
                                                            ? "<span class='status online'>Online</span>"
                                                            : "<span class='status offline'>Offline</span>").'
                                                            <div class="img text-center">
                                                                <img src="'.$site_path.'/images/profile.png"
                                                                    class="img-fluid" alt="">
                                                            </div>
                                                            <div class="name text-center mt-10 fw-bold">
                                                                Ahmed Mohamed
                                                            </div>
                                                            <div class="price text-center text-primary mt-10">
                                                                $1.03 per minute
                                                            </div>
                                                            <div class="about text-center mt-10">
                                                                I am 28 years old, I I\'m working as UX/UI Designer - Front End
                                                                Developer
                                                            </div>
                                                            <div class="tags mt-10">
                                                                <span>#football - $1 per/min</span>
                                                                <span>music</span>
                                                                <span>#video game - $1 per/min</span>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    ';
                                                }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include '../../../components/scripts.php'; ?>
</body>

</html>