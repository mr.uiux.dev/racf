<!DOCTYPE html>
<?php 
    $current_page = 'Dashboard'; 
    $timestamp = date("YmdHis");
    $site_path = 'http://localhost/racf_dashboard/assets';
    $site_url = 'http://localhost/racf_dashboard/views';
    // $site_path = 'https://www.creamyw.com/dev/rent/assets';
    // $site_url = 'https://www.creamyw.com/dev/rent/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../../components/links.php'; ?>
</head>

<body>
    <div class="wrapper">
        <?php include '../../../components/side-menu-member.php'; ?>
        <div class="content__page dashboard__page">
            <div class="container-fluid">
                <?php include '../../../components/header-member.php' ?>
                <div class="py-xl-50 py-lg-40 py-md-30 py-20">
                    <div class="heading mb-lg-35 mb-20">
                        <h1 class="fw-bold text-primary"><?php echo $current_page ?></h1>
                    </div>
                    <div class="row gallery-wrapper overflow-hidden">
                        <div class="col-lg-4 grid-sizer"></div>
                        <div class="col-lg-8 grid-item">
                            <div class="available__balance pt-lg-30 ps-lg-30 pe-lg-0 pt-20 ps-20 text-white">
                                <div class="d-flex flex-column justify-content-between">
                                    <div class="title__price">
                                        <div class="title fw-bold">
                                            Available balance
                                            <img src="<?php echo $site_path ?>/images/stripe.png"
                                                class="img-fluid ms-15" alt="">
                                        </div>
                                        <div class="price fw-bold mt-lg-20 mt-15">
                                            $2,200
                                        </div>
                                    </div>
                                    <div class="text-end mt-lg-50">
                                        <a href="<?php $site_url ?>/member/money" class="btn__custom">Add Money</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 grid-item">
                            <div class="mt-lg-0 my-20">
                                <div class="online__friends card p-lg-20 p-15">
                                    <div class="title fw-bold">
                                        Online Friends
                                    </div>
                                    <div class="friends__list mt-lg-20">
                                        <?php 
                                    for ($i=0; $i < 20; $i++) { 
                                        echo '
                                        <a href="'. $site_url .'/member/cyberprofile/" class="friend__item d-block py-10">
                                            <div class="row align-items-center">
                                                <div class="col-lg-9">
                                                    <div class="profile d-flex">
                                                        <div class="img">
                                                            <img src="'. $site_path .'/images/profile.png"
                                                                class="img-fluid" alt="">
                                                        </div>
                                                        <div class="name__tags ms-10">
                                                            <div class="name">
                                                                John Smith
                                                            </div>
                                                            <div class="tags">
                                                                <span>#football - $1 per/min</span>
                                                                <span>music</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    ';
                                    }
                                    ?>
                                    </div>
                                    <a href="<?php echo $site_url ?>/member/search/" class="btn d-block">See All</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 grid-item  mt-md-30 mt-0">
                            <div class="call__history card p-lg-25 p-15">
                                <div class="title fw-bold">
                                    Call History
                                </div>
                                <div class="history__list mt-lg-25 mt-15">
                                    <?php 
                                    for ($i=0; $i < 5; $i++) { 
                                        echo '
                                        <div class="history__item p-lg-20 p-md-0">
                                            <div class="row align-items-center">
                                                <div class="col-md-6">
                                                    <div class="profile d-flex">
                                                        <div class="img">
                                                            <img src="'. $site_path .'/images/profile.png"
                                                                class="img-fluid" alt="">
                                                        </div>
                                                        <div class="name__tags ms-10">
                                                            <div class="name">
                                                                John Smith
                                                            </div>
                                                            <div class="tags">
                                                                <span>#football - $1 per/min</span>
                                                                <span>music</span>
                                                                <a href="" class="see__more">See More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 text-end mt-md-0 mt-10">
                                                    <div class="actions d-flex align-items-center justify-content-end">
                                                        <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#modalProfile-'.$i.'" class="info">
                                                            <span class="info" data-bs-toggle="tooltip" data-bs-placement="top" title="Show more info">
                                                                <img src="'. $site_path .'/images/icons/info.svg" alt="" >
                                                            </span>
                                                        </a>
                                                        <!-- disbaled -->
                                                        '.(($i === 5 || $i === 2 || $i === 0)
                                                        ? 
                                                        "<a href='' class='video' data-bs-toggle='tooltip' data-bs-placement='top' title='Video Call'>
                                                            <img src='". $site_path ."/images/icons/video.svg' alt=''>
                                                        </a>" 
                                                        : 
                                                        "<a href='' class='video disabled' data-bs-toggle='tooltip' data-bs-placement='top' title='Not available (Offline)'>
                                                            <img src='". $site_path ."/images/icons/video.svg' alt=''>
                                                        </a>"
                                                        ).'
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Modal -->
                                        <div class="modal fade" id="modalProfile-'.$i.'" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <div class="modal-body p-0">
                                                        <div class="row align-items-center">
                                                            <div class="col-md-6">
                                                                <div class="profile d-flex">
                                                                    <div class="img">
                                                                        <img src="'. $site_path .'/images/profile.png"
                                                                            class="img-fluid" alt="">
                                                                    </div>
                                                                    <div class="name__price ms-10">
                                                                        <div class="name fw-bold">
                                                                            John Smith
                                                                        </div>
                                                                        <div class="price text-primary">$1.03 per minute</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 text-end mt-md-0 mt-10">
                                                                <div class="actions d-flex align-items-center justify-content-end">
                                                                    <a href="" class="email" data-bs-toggle="tooltip" data-bs-placement="top" title="Send Email">
                                                                        <img src="'. $site_path .'/images/icons/email.svg" alt="">
                                                                    </a>
                                                                    <!-- disabled -->
                                                                    '.(($i === 5 || $i === 2 || $i === 0)
                                                                    ? 
                                                                    "<a href='' class='video' data-bs-toggle='tooltip' data-bs-placement='top' title='Video Call'>
                                                                        <img src='". $site_path ."/images/icons/video.svg' alt=''>
                                                                    </a>" 
                                                                    : 
                                                                    "<a href='' class='video disabled' data-bs-toggle='tooltip' data-bs-placement='top' title='Not available (Offline)'>
                                                                        <img src='". $site_path ."/images/icons/video.svg' alt=''>
                                                                    </a>"
                                                                    ).'
                                                                </div>
                                                            </div>
                                                            <div class="col-12 mt-10">
                                                                <div class="info">I am 28 years old, I I\'m working as UX/UI Designer - Front End Developer
                                                                </div>
                                                            </div>
                                                            <div class="col-12 mt-10">
                                                                <div class="tags">
                                                                    <span>#football - $1 per/min</span>
                                                                    <span>music</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <div class="data">
                                                                    <ul class="list-unstyled clearfix">
                                                                        <li>
                                                                            <span class="fw-500">Type:</span>
                                                                            <span class="text-primary">Video</span>
                                                                        </li>
                                                                        <li>
                                                                            <span class="fw-500">Date:</span>
                                                                            <span>9 Aug 2021, 08:12:02 (UTC time)
                                                                            </span>
                                                                        </li>
                                                                        <li>
                                                                            <span class="fw-500">Duration:</span>
                                                                            <span>6 mins and 20 secs
                                                                            </span>
                                                                        </li>
                                                                        <li>
                                                                            <span class="fw-500">Status:</span>
                                                                            <!-- answered, unanswered --->
                                                                            '.(($i === 5 || $i === 2 || $i === 0)
                                                                            ? 
                                                                            "<span class='answered'>Competed</span>" 
                                                                            : 
                                                                            "<span class='unanswered'>Unanswered</span>"
                                                                            ).'
                                                                        </li>
                                                                        <li>
                                                                            <span class="fw-500">Spent:</span>
                                                                            <span class="text-primary">$20.25
                                                                            </span>
                                                                        </li>
                                                                        <li>
                                                                            <span class="fw-500">Rates:</span>
                                                                            <span class="star-rating">
                                                                                <span class="star-rating__wrap">
                                                                                    <input class="star-rating__input" id="star-rating-5" type="radio" name="rating" value="5" disabled>
                                                                                    <label class="star-rating__ico fa fa-star-o disabled" for="star-rating-5" title="5 out of 5 stars"></label>
                                                                                    <input class="star-rating__input selected" id="star-rating-4" type="radio" name="rating" value="4" disabled>
                                                                                    <label class="star-rating__ico fa fa-star-o" for="star-rating-4" title="4 out of 5 stars"></label>
                                                                                    <input class="star-rating__input" id="star-rating-3" type="radio" name="rating" value="3" disabled>
                                                                                    <label class="star-rating__ico fa fa-star-o" for="star-rating-3" title="3 out of 5 stars"></label>
                                                                                    <input class="star-rating__input" id="star-rating-2" type="radio" name="rating" value="2" disabled>
                                                                                    <label class="star-rating__ico fa fa-star-o" for="star-rating-2" title="2 out of 5 stars"></label>
                                                                                    <input class="star-rating__input" id="star-rating-1" type="radio" name="rating" value="1" disabled>
                                                                                    <label class="star-rating__ico fa fa-star-o" for="star-rating-1" title="1 out of 5 stars"></label>
                                                                                </span>
                                                                            </span>
                                                                        </li>
                                                                        <li>
                                                                            <span class="fw-500">Comments:</span>
                                                                            <span>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only</span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div class="col-12 mt-10 text-end">
                                                                <a href="'.$site_url.'/member/cyberprofile/" class="btn__custom">See Profile</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ';
                                    }
                                    ?>
                                </div>
                                <div class="text-end pt-20">
                                    <a href="<?php echo $site_url ?>/member/history/"
                                        class="btn__custom d-md-inline-block d-block">See All</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include '../../../components/scripts.php'; ?>
</body>

</html>