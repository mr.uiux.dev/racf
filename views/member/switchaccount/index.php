<!DOCTYPE html>
<?php 
    $current_page = 'Switch'; 
    $timestamp = date("YmdHis");
    $site_path = 'http://localhost/racf_dashboard/assets';
    $site_url = 'http://localhost/racf_dashboard/views';
    // $site_path = 'https://www.creamyw.com/dev/rent/assets';
    // $site_url = 'https://www.creamyw.com/dev/rent/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../../components/links.php'; ?>
</head>

<body>
    <div class="wrapper">
        <?php include '../../../components/side-menu-member.php'; ?>
        <div class="content__page account__page">
            <div class="wrapper">
                <div class="container-fluid">
                    <?php include '../../../components/header-member.php' ?>
                    <div class="py-xl-50 py-lg-40 py-30 py-20">
                        <div class="heading mb-lg-35 mb-20">
                            <h1 class="fw-bold text-primary"><?php echo $current_page ?> Account</h1>
                        </div>
                        <form action="">
                            <div class="form card p-lg-25 p-15 pb-lg-0 mt-md-30 mt-20">
                                <ul class="list-unstyled mb-0 switch__list">
                                    <li>
                                        You can earn money be accepting calls from our members by switching your
                                        account.
                                    </li>
                                    <li>
                                        Your current wallet will stay intact.
                                    </li>
                                    <li>
                                        You can switch back any time.
                                    </li>
                                </ul>
                            </div>
                            <div class="text-end mt-md-30 mt-20">
                                <button type="submit" class="btn__custom">Switch Account</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include '../../../components/scripts.php'; ?>
</body>

</html>