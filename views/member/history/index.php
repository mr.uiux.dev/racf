<!DOCTYPE html>
<?php 
    $current_page = 'History'; 
    $timestamp = date("YmdHis");
    $site_path = 'http://localhost/racf_dashboard/assets';
    $site_url = 'http://localhost/racf_dashboard/views';
    // $site_path = 'https://www.creamyw.com/dev/rent/assets';
    // $site_url = 'https://www.creamyw.com/dev/rent/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../../components/links.php'; ?>
</head>

<body>
    <div class="wrapper">
        <?php include '../../../components/side-menu-member.php'; ?>
        <div class="content__page history__page">
            <div class="wrapper">
                <div class="container-fluid">
                    <?php include '../../../components/header-member.php' ?>
                    <div class="py-xl-50 py-lg-40 py-md-30 py-20">
                        <div class="heading mb-lg-35 mb-20">
                            <h1 class="fw-bold text-primary">Call <?php echo $current_page ?></h1>
                        </div>
                        <div class="mt-md-30 mt-20">
                            <form action="" class="search__history position-relative">
                                <div class="row justify-content-end">
                                    <div class="col-lg-offset-6 col-lg-offset-5 col-lg-6 col-md-7">
                                        <div class="row">
                                            <div class="col-6">
                                                <select name="" id="" class="form-control">
                                                    <option value="">All Calls</option>
                                                </select>
                                            </div>
                                            <div class="col-6">
                                                <select name="" id="" class="form-control">
                                                    <option value="">Descending</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="accordion mt-10" id="historyAccordion">
                                <?php
                                    for ($i=0; $i < 5; $i++) { 
                                        echo'
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="heading'.$i.'">
                                                <button class="accordion-button '.(($i == 0) ? "" : "collapsed").'" type="button" data-bs-toggle="collapse" data-bs-target="#collapse'.$i.'" aria-expanded="'.(($i == 0) ? "true" : "false").'" aria-controls="collapse'.$i.'">
                                                    <div class="row align-items-center w-100">
                                                        <div class="col-md-3 col-12">
                                                            <div class="profile d-flex align-items-center">
                                                                <div class="img">
                                                                    <img src="'. $site_path .'/images/profile.png"
                                                                        class="img-fluid" alt="">
                                                                </div>
                                                                <div class="name ms-10">
                                                                    John Smith
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-12">
                                                            <div class="tags mt-md-0 mt-10">
                                                                <span>#football - $1 per/min</span>
                                                                <span>music</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-12">
                                                            <!-- answered, unanswered -->
                                                            '.(($i === 0 || $i === 3) 
                                                            ? 
                                                            "<span class='status answered d-md-inline-block d-none'>Answered</span>" 
                                                            : 
                                                            "<span class='status unanswered d-md-inline-block d-none'>Unanswered</span>").'
                                                        </div>
                                                        <div class="col-md-2 col-12">
                                                            <span class="date d-md-inline-block d-none">
                                                                9 Aug 2021
                                                                <br>
                                                                05:15:60
                                                            </span>
                                                        </div>
                                                        <div class="col-md-2 col-12">
                                                            <div class="actions mt-md-0 mt-10">
                                                                <!-- disbaled -->
                                                                '.(($i === 5 || $i === 2 || $i === 0)
                                                                ? 
                                                                "<a href='' class='video' data-bs-toggle='tooltip' data-bs-placement='top' title='Video Call'>
                                                                    <img src='". $site_path ."/images/icons/video.svg' alt=''>
                                                                </a>" 
                                                                : 
                                                                "<a href='' class='video disabled' data-bs-toggle='tooltip' data-bs-placement='top' title='Not available (Offline)'>
                                                                    <img src='". $site_path ."/images/icons/video.svg' alt=''>
                                                                </a>"
                                                                ).'
                                                            </div>
                                                        </div> 
                                                    </div>
                                                </button>
                                            </h2>
                                            <div id="collapse'.$i.'" class="accordion-collapse collapse '.(($i == 0) ? "show" : "").'" aria-labelledby="heading'.$i.'" data-bs-parent="#historyAccordion">
                                                <div class="accordion-body">
                                                    <div class="data">
                                                        <ul class="list-unstyled clearfix">
                                                            <li>
                                                                <span class="fw-500">Type:</span>
                                                                <span class="text-primary">Video</span>
                                                            </li>
                                                            <li>
                                                                <span class="fw-500">Date:</span>
                                                                <span>9 Aug 2021, 08:12:02 (UTC time)
                                                                </span>
                                                            </li>
                                                            <li>
                                                                <span class="fw-500">Duration:</span>
                                                                <span>6 mins and 20 secs
                                                                </span>
                                                            </li>
                                                            <li>
                                                                <span class="fw-500">Status:</span>
                                                                <!-- answered, unanswered --->
                                                                '.(($i === 0 || $i === 3)
                                                                ? 
                                                                "<span class='answered'>Competed</span>" 
                                                                : 
                                                                "<span class='unanswered'>Unanswered</span>"
                                                                ).'
                                                            </li>
                                                            <li>
                                                                <span class="fw-500">Spent:</span>
                                                                <span class="text-primary">$20.25
                                                                </span>
                                                            </li>
                                                            <li>
                                                                <span class="fw-500">Rates:</span>
                                                                <span class="star-rating">
                                                                    <span class="star-rating__wrap">
                                                                        <input class="star-rating__input" id="star-rating-5" type="radio" name="rating" value="5" disabled>
                                                                        <label class="star-rating__ico fa fa-star-o disabled" for="star-rating-5" title="5 out of 5 stars"></label>
                                                                        <input class="star-rating__input selected" id="star-rating-4" type="radio" name="rating" value="4" disabled>
                                                                        <label class="star-rating__ico fa fa-star-o" for="star-rating-4" title="4 out of 5 stars"></label>
                                                                        <input class="star-rating__input" id="star-rating-3" type="radio" name="rating" value="3" disabled>
                                                                        <label class="star-rating__ico fa fa-star-o" for="star-rating-3" title="3 out of 5 stars"></label>
                                                                        <input class="star-rating__input" id="star-rating-2" type="radio" name="rating" value="2" disabled>
                                                                        <label class="star-rating__ico fa fa-star-o" for="star-rating-2" title="2 out of 5 stars"></label>
                                                                        <input class="star-rating__input" id="star-rating-1" type="radio" name="rating" value="1" disabled>
                                                                        <label class="star-rating__ico fa fa-star-o" for="star-rating-1" title="1 out of 5 stars"></label>
                                                                    </span>
                                                                </span>
                                                            </li>
                                                            <li>
                                                                <span class="fw-500">Comments:</span>
                                                                <span>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        ';
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include '../../../components/scripts.php'; ?>
</body>

</html>