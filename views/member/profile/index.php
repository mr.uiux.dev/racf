<!DOCTYPE html>
<?php 
    $current_page = 'Profile'; 
    $timestamp = date("YmdHis");
    $site_path = 'http://localhost/racf_dashboard/assets';
    $site_url = 'http://localhost/racf_dashboard/views';
    // $site_path = 'https://www.creamyw.com/dev/rent/assets';
    // $site_url = 'https://www.creamyw.com/dev/rent/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../../components/links.php'; ?>
</head>

<body>
    <div class="wrapper">
        <?php include '../../../components/side-menu-member.php'; ?>
        <div class="content__page profile__page">
            <div class="wrapper">
                <div class="container-fluid">
                    <?php include '../../../components/header-member.php' ?>
                    <div class="py-xl-50 py-lg-40 py-md-30 py-20">
                        <div class="heading mb-lg-35 mb-20">
                            <h1 class="fw-bold text-primary"><?php echo $current_page ?> Image</h1>
                        </div>
                        <form action="">
                            <div class="form card p-lg-25 p-15 mt-md-30 mt-20">
                                <div class="row">
                                    <div class="upload__profile col-12">
                                        <label for="profile">
                                            <input type="file" class="form-control" value="mr.uiux.dev@gmail.com"
                                                name="" id="profile">
                                            <div class="img">
                                                <div
                                                    class="d-flex flex-column align-items-center justify-content-center">
                                                    <span>Upload your profile</span>
                                                    <img src="<?php echo $site_path ?>/images/icons/profile.svg"
                                                        class="svg mt-10" alt="">
                                                </div>
                                            </div>
                                            <!-- Profile Img, show img here after upload -->
                                            <div class="profile__img d-none">
                                                <img src="<?php echo $site_path ?>/images/profile-img.png"
                                                    class="img-fluid" alt="">
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="text-end mt-md-30 mt-20">
                                <button type="submit" class="btn__custom">Upload Image</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include '../../../components/scripts.php'; ?>
</body>

</html>