<!DOCTYPE html>
<?php 
    $current_page = 'Money'; 
    $timestamp = date("YmdHis");
    $site_path = 'http://localhost/racf_dashboard/assets';
    $site_url = 'http://localhost/racf_dashboard/views';
    // $site_path = 'https://www.creamyw.com/dev/rent/assets';
    // $site_url = 'https://www.creamyw.com/dev/rent/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../../components/links.php'; ?>
</head>

<body>
    <div class="wrapper">
        <?php include '../../../components/side-menu-member.php'; ?>
        <div class="content__page money__page">
            <div class="wrapper">
                <div class="container-fluid">
                    <?php include '../../../components/header-member.php' ?>
                    <div class="py-xl-50 py-lg-40 py-md-30 py-20">
                        <div class="heading mb-lg-35 mb-20">
                            <h1 class="fw-bold text-primary">Add <?php echo $current_page ?></h1>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="available__balance p-lg-30 p-md-20 p-md-20 p-10 text-white">
                                    <div class="d-flex flex-column justify-content-between">
                                        <div class="title__price">
                                            <div class="title fw-bold">
                                                Available balance
                                            </div>
                                            <div class="price fw-bold mt-10">
                                                $2,200
                                            </div>
                                        </div>
                                        <div class="text-end mt-lg-50">
                                            <img src="<?php echo $site_path ?>/images/stripe.png"
                                                class="img-fluid ms-15" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="card p-lg-25 p-15 mt-md-0 mt-20">
                                    <div class="prices">
                                        <div class="title fw-bold">
                                            Fixed Prices
                                        </div>
                                        <div class="prices__button d-flex flex-wrap">
                                            <button class="price__item">$10</button>
                                            <button class="price__item">$20</button>
                                            <button class="price__item">$50</button>
                                        </div>
                                    </div>
                                    <div class="amount mt-lg-20 mt-15">
                                        <div class="title fw-bold">
                                            Add Amount
                                        </div>
                                        <form action="">
                                            <input type="text" class="form-control mt-10">
                                        </form>
                                    </div>
                                </div>
                                <div class="text-end mt-md-30 mt-20">
                                    <button type="submit" class="btn__custom">Add Money</button>
                                </div>
                            </div>
                            <div class="col-12 mt-md-30 mt-20">
                                <div class="note p-lg-25 p-15">
                                    <div class="d-flex align-items-start">
                                        <img src="<?php echo $site_path ?>/images/icons/note.svg"
                                            class="me-10 note__icon" alt="">
                                        <p>Payment will be processed through Stripe's secure payment gateway.
                                            <br>
                                            We do not store any credit card information.
                                        </p>
                                    </div>
                                    <div class="text-end">
                                        <img src="<?php echo $site_path ?>/images/stripe-gray.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include '../../../components/scripts.php'; ?>
</body>

</html>