<!DOCTYPE html>
<?php 
    $current_page = 'Invoices'; 
    $timestamp = date("YmdHis");
    $site_path = 'http://localhost/racf_dashboard/assets';
    $site_url = 'http://localhost/racf_dashboard/views';
    // $site_path = 'https://www.creamyw.com/dev/rent/assets';
    // $site_url = 'https://www.creamyw.com/dev/rent/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../../components/links.php'; ?>
</head>

<body>
    <div class="wrapper">
        <?php include '../../../components/side-menu-member.php'; ?>
        <div class="content__page invoices__page">
            <div class="wrapper">
                <div class="container-fluid">
                    <?php include '../../../components/header-member.php' ?>
                    <div class="py-xl-50 py-lg-40 py-md-30 py-20">
                        <div class="heading mb-lg-35 mb-20">
                            <h1 class="fw-bold text-primary"><?php echo $current_page ?></h1>
                        </div>
                        <div class="mt-md-30 mt-20">
                            <div class="invoices__list mt-10">
                                <ul class="list-unstyled clearfix">
                                    <li>
                                        <div class="row">
                                            <div class="col-md col-12 mt-md-0">
                                                <span class="id__num">#</span>
                                            </div>
                                            <div class="col-md col-12 mt-md-0 mt-10">
                                                <span class="amount">1</span>
                                            </div>
                                            <div class="col-md col-12 mt-md-0 mt-10">
                                                <span class="payment__status">Payment Status</span>
                                            </div>
                                            <div class="col-md col-12 mt-md-0 mt-10">
                                                <span class="date">Date</span>
                                            </div>
                                            <div class="col-md col-12 mt-md-0 mt-10">
                                                <span class="receipt">Receipt</span>
                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                            for ($i=1; $i < 20; $i++) { 
                                                echo '
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-md col-12 mt-md-0">
                                                                <span class="id__num">
                                                                        '.$i.'
                                                                </span>
                                                            </div>
                                                            <div class="col-md col-12 mt-md-0 mt-10">
                                                                <span class="amount text-primary" style="width: 1%">$10</span>
                                                            </div>
                                                            <div class="col-md col-12 mt-md-0 mt-10">
                                                                <!-- pending, paid, reject --->
                                                                '.(($i === 3)  
                                                                    ? "<span class='status paid'>Paid</span>" 
                                                                    :  (($i === 5) ? "<span class='status reject'>Reject</span>" 
                                                                    : "<span class='status pending'>Peding</span>")).'
                                                            </div>
                                                            <div class="col-md col-12 mt-md-0 mt-10">
                                                                <span class="date">
                                                                    18 Jul 2021, 08:14:50
                                                                </span>
                                                            </div>
                                                            <div class="col-md col-12 mt-md-0 mt-10">
                                                                <a href="" class="btn-secondary">Details</a>  
                                                            </div>
                                                        </div>
                                                    </li>
                                                ';
                                            }
                                        ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include '../../../components/scripts.php'; ?>
</body>

</html>