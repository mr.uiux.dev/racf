<!DOCTYPE html>
<?php 
    $current_page = 'Cyber Profile'; 
    $timestamp = date("YmdHis");
    $site_path = 'http://localhost/racf/assets';
    $site_url = 'http://localhost/racf/views';
    // $site_path = 'https://www.creamyw.com/dev/rent/assets';
    // $site_url = 'https://www.creamyw.com/dev/rent/views'
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../../components/links.php'; ?>
</head>

<body>
    <div class="wrapper">
        <?php include '../../../components/side-menu-cyber.php'; ?>
        <div class="content__page profile__page">
            <div class="wrapper">
                <div class="container-fluid">
                    <?php include '../../../components/header-cyber.php' ?>
                    <div class="py-xl-50 py-lg-40 py-md-30 py-20">
                        <div class="heading mb-lg-35 mb-20">
                            <h1 class="fw-bold text-primary"><?php echo $current_page ?></h1>
                        </div>
                        <div class="card profile__cyber">
                            <div class="bg"></div>
                            <div class="data position-relative p-md-30 p-20">
                                <div class="actions__social">
                                    <div class="actions d-flex align-items-center justify-content-end">
                                        <a href="" class="email" data-bs-toggle="tooltip" data-bs-placement="top"
                                            title="Send Email">
                                            <img src="<?php echo $site_path ?>/images/icons/email.svg" alt="">
                                        </a>
                                        <!-- disabled -->
                                        <a href='' class='video' data-bs-toggle='tooltip' data-bs-placement='top'
                                            title='Video Call'>
                                            <img src="<?php echo $site_path ?>/images/icons/video.svg" alt=''>
                                        </a>
                                    </div>
                                    <div class="social d-flex align-items-center justify-content-end mt-20">
                                        <a href="" target="_blank">
                                            <i class="fa fa-w fa-facebook"></i>
                                        </a>
                                        <a href="" target="_blank">
                                            <i class="fa fa-w fa-twitter"></i>
                                        </a>
                                        <a href="" target="_blank">
                                            <i class="fa fa-w fa-instagram"></i>
                                        </a>
                                        <a href="" target="_blank">
                                            <i class="fa fa-w fa-linkedin"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="profile">
                                            <div class="img">
                                                <img src="<?php echo $site_path ?>/images/profile.png" class="img-fluid"
                                                    alt="">
                                            </div>
                                        </div>
                                        <div class="name__status d-flex align-items-center mt-10">
                                            <div class="name text-primary fw-bold me-15">Ahmed Mohamed</div>
                                            <!-- online, offline -->
                                            <span class="status online">Online</span>
                                        </div>
                                        <div class="price mt-10">
                                            <span class="text-primary fw-bold">$1.00-$1.00</span> / Per Minute
                                        </div>
                                        <div class="about mt-10">
                                            I am 28 years old, I I'm working as UX/UI Designer - Front End Developer
                                        </div>
                                        <div class="location__languages mt-10">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <a href="" target="_blank" class="d-flex">
                                                        <div class="icon me-10">
                                                            <img src="<?php echo $site_path ?>/images/icons/location.svg"
                                                                alt="">
                                                        </div>
                                                        <div class="mt-1">
                                                            Cairo, Egypt
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="d-flex mt-md-0 mt-10">
                                                        <div class="icon me-10">
                                                            <img src="<?php echo $site_path ?>/images/icons/languages.svg"
                                                                alt="">
                                                        </div>
                                                        <div class="d-flex flex-column mt-1">
                                                            <span>Arabic / <strong>Native</strong></span>
                                                            <span>English / <strong>Near Native -
                                                                    Fluent</strong></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 text-end">
                                        <!-- pinned -->
                                        <button class="btn__custom pin__button">
                                            <d-flex class="align-items-center">
                                                <img src="<?php echo $site_path ?>/images/icons/pin.svg" class="me-10"
                                                    alt="">
                                                Pin Cyber Friend
                                            </d-flex>
                                        </button>
                                    </div>
                                    <div class="col-12">
                                        <hr class="my-20">
                                        <div class="hobbies">
                                            <div class="title text-primary fw-500">
                                                Hobbies
                                            </div>
                                            <div class="tags__name ms-md-20 mt-10">
                                                <div class="row">
                                                    <?php 
                                                        for ($i=0; $i <4 ; $i++) { 
                                                            echo '
                                                            <div class="col-md-6 mt-md-0 mt-10 mb-10">
                                                                <div class="name fw-500">
                                                                    Sports <span class="price text-primary">$1 - $5 <span class="sm-text text-secondary fw-normal">/ Per Minute</span></span>
                                                                </div>
                                                                <div class="tags">
                                                                    <span>#football - $1 per/min</span>
                                                                    <span>music</span>
                                                                </div>
                                                            </div>
                                                            ';
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <hr class="my-20">
                                        <div class="gallery">
                                            <div class="title text-primary fw-bold">
                                                Galleries
                                            </div>
                                            <div class="photos mt-20">
                                                <div class="d-flex flex-wrap justify-content-center">
                                                    <?php 
                                                    for ($i=0; $i < 15; $i++) { 
                                                        echo '
                                                        <div class="photo__item me-md-20 me-10 mb-md-20 mb-10">
                                                            <img src="'. $site_path.'/images/profile-img.png" class="img-fluid" alt="">
                                                        </div>
                                                        ';
                                                    }
                                                ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include '../../../components/scripts.php'; ?>
</body>

</html>