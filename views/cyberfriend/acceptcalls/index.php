<!DOCTYPE html>
<?php 
    $current_page = 'Calls'; 
    $timestamp = date("YmdHis");
    $site_path = 'http://localhost/racf_dashboard/assets';
    $site_url = 'http://localhost/racf_dashboard/views';
    // $site_path = 'https://www.creamyw.com/dev/rent/assets';
    // $site_url = 'https://www.creamyw.com/dev/rent/views'
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../../components/links.php'; ?>
</head>

<body>
    <div class="wrapper">
        <?php include '../../../components/side-menu-cyber.php'; ?>
        <div class="content__page calls__page">
            <div class="wrapper">
                <div class="container-fluid">
                    <?php include '../../../components/header-cyber.php' ?>
                    <div class="py-xl-50 py-lg-40 py-30 py-20">
                        <div class="note p-15">
                            <div class="d-flex align-items-start">
                                <img src="<?php echo $site_path ?>/images/icons/note.svg" class="me-10 note__icon"
                                    alt="">
                                <p class="mb-0">No need to refresh this page.</p>
                            </div>
                        </div>
                        <div class="heading mb-lg-35 mb-20  mt-md-30 mt-20">
                            <h1 class="fw-bold text-primary">Accept Incoming <?php echo $current_page ?></h1>
                        </div>
                        <form action="">
                            <div class="calls__tab card p-lg-25 p-15 pb-lg-0 mt-md-30 mt-20">
                                <ul class="nav nav-pills mb-3 justify-content-center w-100" id="pills-tab"
                                    role="tablist">
                                    <li class="nav-item col" role="presentation">
                                        <!-- online -->
                                        <button class="nav-link w-100 online active" id="pills-online-tab"
                                            data-bs-toggle="pill" data-bs-target="#pills-online" type="button"
                                            role="tab" aria-controls="pills-online" aria-selected="true">
                                            <img src="<?php echo $site_path; ?>/images/icons/video.svg"
                                                class="svg ms-10" alt="">
                                            <span class="d-md-inline-block d-none">Profile Online (Accepting
                                                Calls)</span>
                                            <span class="d-md-none d-inline-block">Online</span>
                                        </button>
                                    </li>
                                    <li class="nav-item col" role="presentation">
                                        <!-- offline -->
                                        <button class="nav-link offline w-100" id="pills-offline-tab"
                                            data-bs-toggle="pill" data-bs-target="#pills-offline" type="button"
                                            role="tab" aria-controls="pills-offline" aria-selected="false">
                                            <img src="<?php echo $site_path; ?>/images/icons/offline.svg"
                                                class="svg ms-10" alt="">
                                            <span class="d-md-inline-block d-none">Profile Offline (Not Accepting
                                                Calls)</span>
                                            <span class="d-md-none d-inline-blokc">Offline</span>
                                        </button>
                                    </li>
                                </ul>
                                <hr class="hr">
                                <div class="tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-online" role="tabpanel"
                                        aria-labelledby="pills-online-tab">
                                        <div class="actions d-flex justify-content-center">
                                            <button class="btn__custom btn__settings me-15" type="button"
                                                data-bs-toggle="collapse" href="#collapseSettings" role="button"
                                                aria-expanded="false" aria-controls="collapseSettings">
                                                <img src="<?php echo $site_path ?>/images/icons/settings.svg"
                                                    class="svg" alt="">
                                            </button>
                                            <!-- mute -->
                                            <button class="btn__custom btn__volume">
                                                <img src="<?php echo $site_path ?>/images/icons/volume.svg" class="svg"
                                                    alt="">
                                            </button>
                                        </div>
                                        <div class="collapse mt-md-30 mt-20" id="collapseSettings">
                                            <div class="card card-body shadow-none">
                                                <div class="row justify-content-center">
                                                    <div class="col-md-3">
                                                        <select name="" id="" class="form-control">
                                                            <option value="">Ringtone Name</option>
                                                            <option value="">Ringtone Name</option>
                                                            <option value="">Ringtone Name</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="actions d-flex">
                                                            <!-- active -->
                                                            <button class="btn__custom btn__play me-15"
                                                                data-bs-toggle="tooltip" data-bs-placement="top"
                                                                title="Play ringtone">
                                                                <img src="<?php echo $site_path ?>/images/icons/play.svg"
                                                                    class="svg icon__play" alt="">
                                                                <img src="<?php echo $site_path ?>/images/icons/pause.svg"
                                                                    class="svg icon__pause" alt="">
                                                            </button>
                                                            <!-- active -->
                                                            <button class="btn__custom btn__bookmark"
                                                                data-bs-toggle="tooltip" data-bs-placement="top"
                                                                title="Save ringtone">
                                                                <img src="<?php echo $site_path ?>/images/icons/bookmark.svg"
                                                                    class="svg" alt="">
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-offline" role="tabpanel"
                                        aria-labelledby="pills-offline-tab">
                                        <div class="title fw-500 text-center">
                                            Your profile is offline, therefore you are not able to accept any calls.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="alerts__list mt-20 d-none">
                            <div class="alert alert-danger mt-10">
                                You’re not able to accept incoming calls until completing your profile. Click <a
                                    href="<?php echo $site_url ?>/cyberfriend/profile">here</a> to complete
                            </div>
                            <div class="alert alert-danger mt-10">
                                You’re not able to accept incoming calls until completing your hobbies. Click <a
                                    href="<?php echo $site_url ?>/cyberfriend/hobbies">here</a> to complete
                            </div>
                            <div class="alert alert-danger mt-10">
                                You’re not able to accept incoming calls until completing your languages. Click <a
                                    href="<?php echo $site_url ?>/cyberfriend/languages">here</a> to complete
                            </div>
                        </div>
                        <div class="incoming__calls card shadow-none bg-transparent mt-20 d-none">
                            <div class="title text-center fw-500">2 Incoming Calls</div>
                            <!-- justify-content-center [for centered] -->
                            <div class="incoming__list row justify-content-center mt-20">
                                <?php
                                    for ($i=0; $i < 3; $i++) { 
                                        echo '
                                        <div class="col-lg-4 col-md-6">
                                            <div class="incoming__item p-15">
                                                <div class="profile d-flex justify-content-between">
                                                    <div class="name__price__tags text-secondary">
                                                        <div class="name fw-500">Ahmed Mohamed</div>
                                                        <div class="price">Price <span class="text-primary">1$ / Per Minute</span></div>
                                                        <div class="tags">
                                                            <span>#football - $1 per/min</span>
                                                            <span>music</span>
                                                        </div>
                                                    </div>
                                                    <div class="img">
                                                        <img src="'.$site_path.'/images/profile.png" class="img-fluid" alt="">
                                                    </div>
                                                </div>
                                                <div class="actions d-flex justify-content-between mt-20">
                                                    <button class="btn__custom btn__delcine">
                                                        <img src="'.$site_path.'/images/icons/history.svg" class="svg" alt="">
                                                    </button>
                                                    <button class="btn__custom btn__answer">
                                                        <img src="'.$site_path.'/images/icons/video.svg" class="svg" alt="">
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        ';
                                    }
                               ?>
                            </div>
                        </div>
                        <div
                            class="waiting__calls d-flex align-items-center justify-content-center flex-column text-center mt-20">
                            <div class="img">
                                <img src="<?php echo $site_path ?>/images/coming-call.svg" class="img-fluid" alt="">
                            </div>
                            <div class="heading text-primary mt-2">
                                <h3>Thanks for your patience</h3>
                            </div>
                            <div class="desc mt-2">
                                <p>Incoming calls will come during:
                                    <span class="timer fw-bold text-primary">
                                        25
                                    </span>
                                </p>
                            </div>
                            <div class="loader mt-25">
                                <?php 
                                    for ($i=0; $i < 26; $i++) { 
                                        echo '<div class="dot"></div>';
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include '../../../components/scripts.php'; ?>
</body>

</html>