<!DOCTYPE html>
<?php 
    $current_page = 'Switch'; 
    $timestamp = date("YmdHis");
    $site_path = 'http://localhost/racf/assets';
    $site_url = 'http://localhost/racf/views';
    // $site_path = 'https://www.creamyw.com/dev/rent/assets';
    // $site_url = 'https://www.creamyw.com/dev/rent/views'
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../../components/links.php'; ?>
</head>

<body>
    <div class="wrapper">
        <?php include '../../../components/side-menu-cyber.php'; ?>
        <div class="content__page account__page">
            <div class="wrapper">
                <div class="container-fluid">
                    <?php include '../../../components/header-cyber.php' ?>
                    <div class="py-xl-50 py-lg-40 py-30 py-20">
                        <div class="heading mb-lg-35 mb-20">
                            <h1 class="fw-bold text-primary"><?php echo $current_page ?> Account</h1>
                        </div>
                        <form action="">
                            <div class="form card p-lg-25 p-15 pb-lg-0 mt-md-30 mt-20">
                                <ul class="list-unstyled mb-0 switch__list">
                                    <li>
                                        You can rent a Cyber Friend by switching your account. By doing so, you will be
                                        able to search and call Cyber Friends.
                                    </li>
                                    <li>
                                        Your current earnings will stay intact.
                                    </li>
                                    <li>
                                        You can switch back any time.
                                    </li>
                                </ul>
                                <div class="note mt-15 p-lg-25 p-15">
                                    <div class="d-flex align-items-start">
                                        <img src="<?php echo $site_path ?>/images/icons/note.svg"
                                            class="me-10 note__icon" alt="">
                                        <p class="mb-0">
                                            Please note, after switching accounts, your current earning cannot to be
                                            spent on call with Cyber Friends at the moment, but you can always add money
                                            to your wallet.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="text-end mt-md-30 mt-20">
                                <button type="submit" class="btn__custom">Switch Account</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include '../../../components/scripts.php'; ?>
</body>

</html>