<!DOCTYPE html>
<?php 
    $current_page = 'Social'; 
    $timestamp = date("YmdHis");
    $site_path = 'http://localhost/racf/assets';
    $site_url = 'http://localhost/racf/views';
    // $site_path = 'https://www.creamyw.com/dev/rent/assets';
    // $site_url = 'https://www.creamyw.com/dev/rent/views'
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../../components/links.php'; ?>
</head>

<body>
    <div class="wrapper">
        <?php include '../../../components/side-menu-cyber.php'; ?>
        <div class="content__page social__page">
            <div class="wrapper">
                <div class="container-fluid">
                    <?php include '../../../components/header-cyber.php' ?>
                    <div class="py-xl-50 py-lg-40 py-30 py-20">
                        <div class="alert alert-danger my-20">
                            Note: after making any changes your RACF profile will be unpublished and published only
                            after by the approval of one of our administrators
                        </div>
                        <div class="heading mb-lg-35 mb-20">
                            <h1 class="fw-bold text-primary"><?php echo $current_page ?> Links</h1>
                        </div>
                        <form action="">
                            <div class="form card p-lg-25 p-15 pb-lg-0 mt-md-30 mt-20">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="facebook">Facebook</label>
                                        <input type="text" class="form-control" name="" id="facebook">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="twitter">Twitter</label>
                                        <input type="text" class="form-control" name="" id="twitter">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="instagram">Instagram</label>
                                        <input type="text" class="form-control" name="" id="instagram">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="linkedin">Linkedin</label>
                                        <input type="text" class="form-control" name="" id="linkedin">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="tiktok">Tiktok</label>
                                        <input type="text" class="form-control" name="" id="tiktok">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="website">Website</label>
                                        <input type="text" class="form-control" name="" id="website">
                                    </div>
                                </div>
                            </div>
                            <div class="text-end mt-md-30 mt-20">
                                <button type="submit" class="btn__custom">Save Changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include '../../../components/scripts.php'; ?>
</body>

</html>