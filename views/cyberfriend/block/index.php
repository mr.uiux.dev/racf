<!DOCTYPE html>
<?php 
    $current_page = 'Block'; 
    $timestamp = date("YmdHis");
    $site_path = 'http://localhost/racf/assets';
    $site_url = 'http://localhost/racf/views';
    // $site_path = 'https://www.creamyw.com/dev/rent/assets';
    // $site_url = 'https://www.creamyw.com/dev/rent/views'
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../../components/links.php'; ?>
</head>

<body>
    <div class="wrapper">
        <?php include '../../../components/side-menu-cyber.php'; ?>
        <div class="content__page money__page">
            <div class="wrapper">
                <div class="container-fluid">
                    <?php include '../../../components/header-cyber.php' ?>
                    <div class="py-xl-50 py-lg-40 py-md-30 py-20">
                        <div class="heading mb-lg-35 mb-20">
                            <h1 class="fw-bold text-primary"><?php echo $current_page ?> Users</h1>
                        </div>
                        <div class="block__list mt-10">
                            <ul class="list-unstyled clearfix">
                                <?php
                                    for ($i=1; $i < 20; $i++) { 
                                        echo '
                                            <li>
                                                <div class="row align-items-center">
                                                    <div class="col">
                                                        <div class="profile d-flex align-items-center">
                                                            <div class="img">
                                                                <img src="'. $site_path .'/images/profile.png"
                                                                    class="img-fluid" alt="">
                                                            </div>
                                                            <div class="name ms-10">
                                                                John Smith
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col text-end">
                                                        <button type="submit" class="btn__delete p-0 bg-transparent border-0">
                                                            <img src="'.$site_path.'/images/icons/delete.svg" alt="">
                                                        </button>  
                                                    </div>
                                                </div>
                                            </li>
                                        ';
                                    }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include '../../../components/scripts.php'; ?>
</body>

</html>