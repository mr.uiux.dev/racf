<!DOCTYPE html>
<?php 
    $current_page = 'Languages'; 
    $timestamp = date("YmdHis");
    $site_path = 'http://localhost/racf/assets';
    $site_url = 'http://localhost/racf/views';
    // $site_path = 'https://www.creamyw.com/dev/rent/assets';
    // $site_url = 'https://www.creamyw.com/dev/rent/views'
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../../components/links.php'; ?>
</head>

<body>
    <div class="wrapper">
        <?php include '../../../components/side-menu-cyber.php'; ?>
        <div class="content__page languages__page">
            <div class="wrapper">
                <div class="container-fluid">
                    <?php include '../../../components/header-cyber.php' ?>
                    <div class="py-xl-50 py-lg-40 py-30 py-20">
                        <div class="alert alert-danger my-20">
                            Note: after making any changes your RACF profile will be unpublished and published only
                            after by the approval of one of our administrators
                        </div>
                        <div class="heading mb-lg-35 mb-20">
                            <h1 class="fw-bold text-primary"><?php echo $current_page ?></h1>
                        </div>
                        <form action="">
                            <div class="form card p-lg-25 p-15 pb-lg-0 mt-md-30 mt-20">
                                <div class="row">
                                    <div class="col-12">
                                        <label for="setupLanguages" class="form-label">Setup Languages</label>
                                        <input class="form-control" list="datalistOptions" id="setupLanguages"
                                            placeholder="Type to languages">
                                        <datalist id="datalistOptions">
                                            <option value="English">
                                            <option value="Arabic">
                                        </datalist>
                                    </div>
                                    <div class="col-12">
                                        <ul class="languages__list list-unstyled clearfix p-lg-25 p-15 pb-lg-0 mt-10"
                                            id="sortableLanguages">
                                            <?php
                                                for ($i=0; $i < 3; $i++) { 
                                                    echo '
                                                    <li class="language__item">
                                                       <div class="d-flex align-items-center flex-wrap justify-content-between">
                                                            <div class="language__selection d-flex align-items-center flex-md-nowrap flex-wrap">
                                                                <div class="sort__icon me-md-20 me-10">
                                                                    <img src="'.$site_path.'/images/icons/sort.svg" alt="">
                                                                </div>
                                                                <div class="name text-primary me-md-20 me-10">
                                                                    English
                                                                </div>
                                                                <div class="status me-md-20 me-10">
                                                                    <select class="form-control">
                                                                        <option value="native" selected="">Native</option>
                                                                        <option value="near_native">Near Native/ fluent</option>
                                                                        <option value="proficient">Proficient</option>
                                                                        <option value="basic">Basic</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <button type="submit" class="btn__delete p-0 bg-transparent border-0">
                                                                <img src="'.$site_path.'/images/icons/delete.svg" alt="">
                                                            </button>
                                                       </div>
                                                    </li>
                                                    ';
                                                    }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="text-end mt-md-30 mt-20">
                                <button type="submit" class="btn__custom">Save Changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include '../../../components/scripts.php'; ?>
</body>

</html>