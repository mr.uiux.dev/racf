<!DOCTYPE html>
<?php 
    $current_page = 'Dashboard'; 
    $timestamp = date("YmdHis");
    $site_path = 'http://localhost/racf/assets';
    $site_url = 'http://localhost/racf/views';
    // $site_path = 'https://www.creamyw.com/dev/rent/assets';
    // $site_url = 'https://www.creamyw.com/dev/rent/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../../components/links.php'; ?>
</head>

<body>
    <div class="wrapper">
        <?php include '../../../components/side-menu-cyber.php'; ?>
        <div class="content__page dashboard__page">
            <div class="container-fluid">
                <?php include '../../../components/header-cyber.php' ?>
                <div class="py-xl-50 py-lg-40 py-md-30 py-20">
                    <div class="heading mb-lg-35 mb-20">
                        <h1 class="fw-bold text-primary"><?php echo $current_page ?></h1>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="available__balance pt-lg-30 ps-lg-30 pe-lg-0 pt-20 ps-20 text-white">
                                <div class="d-flex flex-column justify-content-between">
                                    <div class="title__price">
                                        <div class="title fw-bold">
                                            Available balance
                                            <img src="<?php echo $site_path ?>/images/stripe.png"
                                                class="img-fluid ms-15" alt="">
                                        </div>
                                        <div class="price fw-bold mt-lg-20 mt-15">
                                            $2,200
                                        </div>
                                    </div>
                                    <div class="text-end mt-lg-50">
                                        <a href="<?php $site_url ?>/member/money" class="btn__custom">Add Money</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 mt-lg-0 mt-md-20 mt-20">
                            <div class="online__users h-100 d-flex flex-column justify-content-center p-lg-25 p-15">
                                <div class="icon">
                                    <img src="<?php echo $site_path ?>/images/icons/online-users.svg" alt="">
                                </div>
                                <div class="title mt-md-20 mt-10 fw-bold">
                                    Online Users
                                </div>
                                <div class="num fw-bold mt-md-20 mt-10">100</div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 mt-lg-0 mt-md-20 mt-20">
                            <div class="followers h-100 d-flex flex-column justify-content-center p-lg-25 p-15">
                                <div class="icon">
                                    <img src="<?php echo $site_path ?>/images/icons/followers.svg" alt="">
                                </div>
                                <div class="title mt-md-20 mt-10 fw-bold">
                                    Followers
                                </div>
                                <div class="num fw-bold mt-md-20 mt-10">1500</div>
                            </div>
                        </div>
                        <div class="col-12 mt-md-30 mt-20">
                            <div class="public__page card p-lg-15 p-10">
                                <div class="title fw-bold">
                                    This is your public page url:
                                </div>
                                <div
                                    class="link__profile d-flex justify-content-between align-items-center flex-wrap mt-md-25 mt-15 p-15">
                                    <a href="<?php echo $site_url ?>/cyberfriend/cyberprofile/"
                                        class="text-white text__copy">
                                        https://www.racf.me/v1/fSnGiduHwYUk3zz/
                                    </a>
                                    <button
                                        class="btn__custom btn__copy sm-text d-md-inline-block d-block mt-md-0 mt-10"
                                        ata-bs-container="body" data-bs-toggle="popover" data-bs-placement="top"
                                        data-bs-content="Copied Link">
                                        <img src="<?php echo $site_path ?>/images/icons/copy.svg" class="svg me-10"
                                            alt=""> <span class="d-md-inline-block d-non">Copy Link</span>
                                    </button>
                                    <input type="hidden" name="">
                                </div>
                                <div class="profile__url mt-md-25 mt-15">
                                    <button class="w-100 p-15 accordion__button collapse show" data-bs-toggle="collapse"
                                        href="#profileUrl" role="button" aria-expanded="true"
                                        aria-controls="profileUrl">
                                        Change your profile page url?
                                    </button>
                                    <div class="collapse show" id="profileUrl">
                                        <div class="m-15 pt-15 border-top d-flex align-items-center">
                                            https://www.racf.me/v1/ <form action="" class="mx-10"><input type="text"
                                                    class="form-control"></form> /
                                        </div>
                                        <div class="note m-15 p-lg-25 p-15">
                                            <div class="d-flex align-items-start">
                                                <img src="<?php echo $site_path ?>/images/icons/note.svg"
                                                    class="me-10 note__icon" alt="">
                                                <p class="mb-0">Must not contain spaces or any special other character
                                                    such as >, ", &, @, # e.t.c. Can contain only dashes and dots that
                                                    are not in a row.
                                                    <br>
                                                    Min chars 5.
                                                    <br>
                                                    Must not exceed 50 chars.
                                                    <br>
                                                    Must be unique.
                                                    <br>
                                                    Must not contain any bad words (sex,insults, e.t.c.).
                                                </p>
                                            </div>
                                        </div>
                                        <div class="m-15 d-flex justify-content-end align-items-center flex-wrap">
                                            <div
                                                class="alert alert-danger alert__sm mb-md-0 mb-10 me-sm-10 me-0 d-md-inline-block d-block">
                                                This can be done only once.
                                            </div>
                                            <button type="submit" class="btn__custom">
                                                Send Request
                                            </button>
                                        </div>
                                        <p class="m-15 mb-0 sm-text">
                                            After having sent the request, our admins will check it and you will get a
                                            response here whether it is approved or not.
                                        </p>
                                    </div>
                                </div>
                                <div class="searchable mt-md-25 mt-15 p-15">
                                    <div class="d-flex align-items-center justify-content-between flex-wrap">
                                        <div class="title">
                                            Profile searchable <br>
                                            <span class="sm-text">(get displayed or not in the results search
                                                page)</span>
                                        </div>
                                        <form action="">
                                            <select name="" id="" class="form-control mt-md-0 mt-10">
                                                <option value="">True</option>
                                                <option value="">False</option>
                                            </select>
                                        </form>
                                    </div>
                                </div>

                                <div class="profile__colors mt-md-25 mt-15">
                                    <button class="w-100 p-15 accordion__button collapse show" data-bs-toggle="collapse"
                                        href="#profileColors" role="button" aria-expanded="true"
                                        aria-controls="profileColors">
                                        <div class="d-flex flex-column">
                                            Profile colors <br>
                                            <span class="sm-text">(get displayed or not in the results search
                                                page)</span>
                                        </div>
                                    </button>
                                    <div class="collapse show" id="profileColors">
                                        <div class="m-15 pt-15 border-top d-flex align-items-center">
                                            <ul class="clearfix list-unstyled w-100">
                                                <?php 
                                                    for ($i=0; $i < 7; $i++) { 
                                                        echo '
                                                        <li class="d-flex algin-item-center justify-content-between">
                                                            <div class="form-check">
                                                                <input class="form-check-input" '.(($i === 0) ? "checked" : "").' type="radio" name="colorName" id="colorName'.$i.'">
                                                                <label class="form-check-label" for="colorName'.$i.'">
                                                                    ColorName
                                                                </label>
                                                            </div>
                                                            <div class="box__color d-flex">
                                                                 '.(($i === 0) 
                                                                    ? " <div class='color__item' style='background:#1EB5D8'></div>
                                                                        <div class='color__item' style='background:#fff'></div>"
                                                                    : (($i === 1) ?
                                                                        " <div class='color__item' style='background:#1C5F89'></div>
                                                                        <div class='color__item' style='background:#fff'></div>"
                                                                    : (($i === 2) ?
                                                                        " <div class='color__item' style='background:#E95420'></div>
                                                                        <div class='color__item' style='background:#fff'></div>"
                                                                    : (($i === 2) ?
                                                                        " <div class='color__item' style='background:#2F2F2F'></div>
                                                                        <div class='color__item' style='background:#fff'></div>"
                                                                    : (($i === 3) ?
                                                                        " <div class='color__item' style='background:#FF1414'></div>
                                                                        <div class='color__item' style='background:#fff'></div>"
                                                                    : (($i === 4) ?
                                                                        " <div class='color__item' style='background:#FF14CC'></div>
                                                                        <div class='color__item' style='background:#fff'></div>"
                                                                    : (($i === 4) ?
                                                                        " <div class='color__item' style='background:#FFF700'></div>
                                                                        <div class='color__item' style='background:#15171D'></div>"
                                                                    : (($i === 5) ?
                                                                        " <div class='color__item' style='background:#fff'></div>
                                                                        <div class='color__item' style='background:#15171D'></div>"
                                                                    : (($i === 6) ?
                                                                        " <div class='color__item' style='background:#33E63F'></div>
                                                                        <div class='color__item' style='background:#fff'></div>"
                                                                    : (($i === 7) ?
                                                                        " <div class='color__item' style='background:#762A77'></div>
                                                                        <div class='color__item' style='background:#fff'></div>"
                                                                    : ""
                                                                    )))))))))).'
                                                            </div>
                                                        </li>
                                                        ';
                                                    }
                                                ?>
                                                <li class="d-flex algin-item-center justify-content-between">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="colorName"
                                                            id="colorName8">
                                                        <label class="form-check-label" for="colorName8">
                                                            Custom Color
                                                        </label>
                                                    </div>
                                                    <div class="box__color d-flex">
                                                        <input type="color" class="color__item" value="#1EB5D8"></input>
                                                        <input type="color" class="color__item" value="#ffffff"></input>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="text-end m-15">
                                            <button type="submit" class="btn__custom">Update</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include '../../../components/scripts.php'; ?>
</body>

</html>