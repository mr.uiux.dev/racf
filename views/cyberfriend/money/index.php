<!DOCTYPE html>
<?php 
    $current_page = 'Money'; 
    $timestamp = date("YmdHis");
    $site_path = 'http://localhost/racf/assets';
    $site_url = 'http://localhost/racf/views';
    // $site_path = 'https://www.creamyw.com/dev/rent/assets';
    // $site_url = 'https://www.creamyw.com/dev/rent/views'
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../../components/links.php'; ?>
</head>

<body>
    <div class="wrapper">
        <?php include '../../../components/side-menu-cyber.php'; ?>
        <div class="content__page money__page">
            <div class="wrapper">
                <div class="container-fluid">
                    <?php include '../../../components/header-cyber.php' ?>
                    <div class="py-xl-50 py-lg-40 py-md-30 py-20">
                        <div class="row">
                            <div class="col-12">
                                <div class="heading mb-lg-35 mb-20">
                                    <h1 class="fw-bold text-primary">Withdraw <?php echo $current_page ?></h1>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="available__balance ps-lg-30  pt-lg-30 ps-20 pt-20 text-white">
                                    <div class="d-flex flex-column justify-content-between">
                                        <div class="title__price">
                                            <div class="title fw-bold d-flex justify-content-between">
                                                Available balance
                                            </div>
                                            <div class="price fw-bold mt-10">
                                                $2,200
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="title heading mt-lg-30 mt-20">
                            <h2 class="fw-bold text-primary">Payment Methods</h2>
                        </div>
                        <div class="accordion mt-lg-30 mt-20" id="accordionMoney">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Bank Transfer
                                    </button>
                                </h2>
                                <div id="collapseOne" class="accordion-collapse collapse show"
                                    aria-labelledby="headingOne" data-bs-parent="#accordionMoney">
                                    <div class="accordion-body">
                                        <div class="card p-lg-25 p-15 mt-md-30 mt-20">
                                            <div class="form">
                                                <div class="title fw-bold mb-md-30 mb-20">
                                                    In order to request money withdrawals you must complete your bank
                                                    account
                                                    information:
                                                </div>
                                                <form action="">
                                                    <div class="form-group">
                                                        <label for="iban">IBAN</label>
                                                        <input type="text" name="" id="iban" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="name">Name of the account’s beneficiary</label>
                                                        <input type="text" name="" id="name" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="bic">BIC Code/Swift</label>
                                                        <input type="text" name="" id="bic" class="form-control">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="text-end mt-md-30 mt-20">
                                            <button type="submit" class="btn__custom">Save Changes</button>
                                        </div>
                                        <div class="amount__transfer">
                                            <div class="card p-lg-25 p-15 mt-md-30 mt-20">
                                                <div class="form">
                                                    <form action="">
                                                        <div class="form-group mb-0">
                                                            <label for="bic">Amount Money</label>
                                                            <input type="text" name="" id="bic" class="form-control">
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="text-end mt-md-30 mt-20">
                                                <button type="submit" class="btn__custom">Request A Withdrawal</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Stripe
                                    </button>
                                </h2>
                                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                                    data-bs-parent="#accordionMoney">
                                    <div class="accordion-body">
                                        <div
                                            class="available__balance stripe__balance ps-lg-30 pt-lg-30 ps-20 pt-20 text-white">
                                            <div class="d-flex flex-column justify-content-between">
                                                <div class="title__price">
                                                    <div class="title fw-bold d-flex justify-content-between">
                                                        Stripe
                                                        <img src="<?php echo $site_path ?>/images/stripe.png"
                                                            class="img-fluid me-lg-30 ps-md-20" alt="">
                                                    </div>
                                                    <div class="price fw-bold mt-10">
                                                        Coming Soon
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseThree" aria-expanded="false"
                                        aria-controls="collapseThree">
                                        PayPal
                                    </button>
                                </h2>
                                <div id="collapseThree" class="accordion-collapse collapse"
                                    aria-labelledby="headingThree" data-bs-parent="#accordionMoney">
                                    <div class="accordion-body">
                                        <div
                                            class="available__balance paypal__balance ps-lg-30  pt-lg-30 ps-20 pt-20 text-white">
                                            <div class="d-flex flex-column justify-content-between">
                                                <div class="title__price">
                                                    <div class="title fw-bold d-flex justify-content-between">
                                                        Paypal
                                                        <img src="<?php echo $site_path ?>/images/paypal.png"
                                                            class="img-fluid me-lg-30 ps-md-20" alt="">
                                                    </div>
                                                    <div class="price fw-bold mt-10">
                                                        Coming Soon
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="hr">
                        <div class="title heading mt-lg-30 mt-20">
                            <h2 class="fw-bold text-primary">Transactions</h2>
                        </div>
                        <div class="invoices__list mt-lg-30 mt-20">
                            <ul class="list-unstyled clearfix">
                                <li>
                                    <div class="row">
                                        <div class="col-md col-12 mt-md-0">
                                            <span class="id__num">#</span>
                                        </div>
                                        <div class="col-md col-12 mt-md-0 mt-10">
                                            <span class="amount">1</span>
                                        </div>
                                        <div class="col-md col-12 mt-md-0 mt-10">
                                            <span class="payment__status">Payment Status</span>
                                        </div>
                                        <div class="col-md col-12 mt-md-0 mt-10">
                                            <span class="date">Date</span>
                                        </div>
                                        <div class="col-md col-12 mt-md-0 mt-10">
                                            <span class="receipt">Receipt</span>
                                        </div>
                                    </div>
                                </li>
                                <?php
                                        for ($i=1; $i < 20; $i++) { 
                                            echo '
                                                <li>
                                                    <div class="row">
                                                        <div class="col-md col-12 mt-md-0">
                                                            <span class="id__num">
                                                                    '.$i.'
                                                            </span>
                                                        </div>
                                                        <div class="col-md col-12 mt-md-0 mt-10">
                                                            <span class="amount text-primary" style="width: 1%">$10</span>
                                                        </div>
                                                        <div class="col-md col-12 mt-md-0 mt-10">
                                                            <!-- pending, paid, reject --->
                                                            '.(($i === 3)  
                                                                ? "<span class='status paid'>Paid</span>" 
                                                                :  (($i === 5) ? "<span class='status reject'>Reject</span>" 
                                                                : "<span class='status pending'>Peding</span>")).'
                                                        </div>
                                                        <div class="col-md col-12 mt-md-0 mt-10">
                                                            <span class="date">
                                                                18 Jul 2021, 08:14:50
                                                            </span>
                                                        </div>
                                                        <div class="col-md col-12 mt-md-0 mt-10">
                                                            <a href="" class="btn-secondary">Details</a>  
                                                        </div>
                                                    </div>
                                                </li>
                                            ';
                                        }
                                    ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include '../../../components/scripts.php'; ?>
</body>

</html>