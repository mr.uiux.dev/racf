<!DOCTYPE html>
<?php 
    $current_page = 'Money'; 
    $timestamp = date("YmdHis");
    $site_path = 'http://localhost/racf/assets';
    $site_url = 'http://localhost/racf/views';
    // $site_path = 'https://www.creamyw.com/dev/rent/assets';
    // $site_url = 'https://www.creamyw.com/dev/rent/views'
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../../components/links.php'; ?>
</head>

<body>
    <div class="wrapper">
        <?php include '../../../components/side-menu-cyber.php'; ?>
        <div class="content__page money__page">
            <div class="wrapper">
                <div class="container-fluid">
                    <?php include '../../../components/header-cyber.php' ?>
                    <div class="py-xl-50 py-lg-40 py-md-30 py-20">
                        <div class="row">
                            <div class="col-12">
                                <div class="heading mb-lg-35 mb-20">
                                    <h1 class="fw-bold text-primary">Withdraw <?php echo $current_page ?></h1>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="available__balance ps-lg-30  pt-lg-30 ps-20 pt-20 text-white">
                                    <div class="d-flex flex-column justify-content-between">
                                        <div class="title__price">
                                            <div class="title fw-bold d-flex justify-content-between">
                                                Available balance
                                            </div>
                                            <div class="price fw-bold mt-10">
                                                $2,200
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="title heading mt-lg-30 mt-20">
                                    <h2 class="fw-bold text-primary">Payment Methods</h2>
                                </div>
                                <div class="card p-lg-15 p-10 mt-lg-30 mt-20">
                                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <button class="nav-link active" id="pills-bank-tab" data-bs-toggle="pill"
                                                data-bs-target="#pills-bank" type="button" role="tab"
                                                aria-controls="pills-bank" aria-selected="true">Bank Transfer</button>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <button class="nav-link" id="pills-paypal-tab" data-bs-toggle="pill"
                                                data-bs-target="#pills-paypal" type="button" role="tab"
                                                aria-controls="pills-paypal" aria-selected="false">PayPal</button>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <button class="nav-link" id="pills-stripe-tab" data-bs-toggle="pill"
                                                data-bs-target="#pills-stripe" type="button" role="tab"
                                                aria-controls="pills-stripe" aria-selected="false">Stripe</button>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="pills-tabContent">
                                        <div class="tab-pane fade show active" id="pills-bank" role="tabpanel"
                                            aria-labelledby="pills-bank-tab">
                                            <div class="form pt-20">
                                                <div class="title fw-bold mb-md-30 mb-20">
                                                    In order to request money withdrawals you must complete your bank
                                                    account
                                                    information:
                                                </div>
                                                <form action="">
                                                    <div class="form-group">
                                                        <label for="iban">IBAN</label>
                                                        <input type="text" name="" id="iban" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="name">Name of the account’s beneficiary</label>
                                                        <input type="text" name="" id="name" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="bic">BIC Code/Swift</label>
                                                        <input type="text" name="" id="bic" class="form-control">
                                                    </div>
                                                    <div class="text-end mt-md-30 mt-20">
                                                        <button type="submit" class="btn__custom">Save Changes</button>
                                                    </div>
                                                </form>
                                                <hr class="hr">
                                                <div class="form">
                                                    <form action="">
                                                        <div class="form-group mb-0">
                                                            <label for="bic">Amount Money</label>
                                                            <input type="text" name="" id="bic" class="form-control">
                                                        </div>
                                                        <div class="text-end mt-md-30 mt-20">
                                                            <button type="submit" class="btn__custom">Request A
                                                                Withdrawal</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="pills-paypal" role="tabpanel"
                                            aria-labelledby="pills-paypal-tab">
                                            <div
                                                class="available__balance paypal__balance ps-lg-30  pt-lg-30 ps-20 pt-20 text-white">
                                                <div class="d-flex flex-column justify-content-between">
                                                    <div class="title__price">
                                                        <div class="title fw-bold d-flex justify-content-between">
                                                            Paypal
                                                            <img src="<?php echo $site_path ?>/images/paypal.png"
                                                                class="img-fluid me-lg-30 ps-md-20" alt="">
                                                        </div>
                                                        <div class="price fw-bold mt-10">
                                                            Coming Soon
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="pills-stripe" role="tabpanel"
                                            aria-labelledby="pills-stripe-tab">
                                            <div
                                                class="available__balance stripe__balance ps-lg-30 pt-lg-30 ps-20 pt-20 text-white">
                                                <div class="d-flex flex-column justify-content-between">
                                                    <div class="title__price">
                                                        <div class="title fw-bold d-flex justify-content-between">
                                                            Stripe
                                                            <img src="<?php echo $site_path ?>/images/stripe.png"
                                                                class="img-fluid me-lg-30 ps-md-20" alt="">
                                                        </div>
                                                        <div class="price fw-bold mt-10">
                                                            Coming Soon
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="hr">
                        <div class="title heading mt-lg-30 mt-20">
                            <h2 class="fw-bold text-primary">Payment Methods</h2>
                        </div>
                        <div class="invoices__list mt-lg-30 mt-20">
                            <ul class="list-unstyled clearfix">
                                <li>
                                    <div class="row">
                                        <div class="col-md col-12 mt-md-0">
                                            <span class="id__num">#</span>
                                        </div>
                                        <div class="col-md col-12 mt-md-0 mt-10">
                                            <span class="amount">1</span>
                                        </div>
                                        <div class="col-md col-12 mt-md-0 mt-10">
                                            <span class="payment__status">Payment Status</span>
                                        </div>
                                        <div class="col-md col-12 mt-md-0 mt-10">
                                            <span class="date">Date</span>
                                        </div>
                                        <div class="col-md col-12 mt-md-0 mt-10">
                                            <span class="receipt">Receipt</span>
                                        </div>
                                    </div>
                                </li>
                                <?php
                                        for ($i=1; $i < 20; $i++) { 
                                            echo '
                                                <li>
                                                    <div class="row">
                                                        <div class="col-md col-12 mt-md-0">
                                                            <span class="id__num">
                                                                    '.$i.'
                                                            </span>
                                                        </div>
                                                        <div class="col-md col-12 mt-md-0 mt-10">
                                                            <span class="amount text-primary" style="width: 1%">$10</span>
                                                        </div>
                                                        <div class="col-md col-12 mt-md-0 mt-10">
                                                            <!-- pending, paid, reject --->
                                                            '.(($i === 3)  
                                                                ? "<span class='status paid'>Paid</span>" 
                                                                :  (($i === 5) ? "<span class='status reject'>Reject</span>" 
                                                                : "<span class='status pending'>Peding</span>")).'
                                                        </div>
                                                        <div class="col-md col-12 mt-md-0 mt-10">
                                                            <span class="date">
                                                                18 Jul 2021, 08:14:50
                                                            </span>
                                                        </div>
                                                        <div class="col-md col-12 mt-md-0 mt-10">
                                                            <a href="" class="btn-secondary">Details</a>  
                                                        </div>
                                                    </div>
                                                </li>
                                            ';
                                        }
                                    ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include '../../../components/scripts.php'; ?>
</body>

</html>