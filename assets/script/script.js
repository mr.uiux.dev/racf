$(() => {
	// Tooltip
	var tooltipTriggerList = [].slice.call(
		document.querySelectorAll('[data-bs-toggle="tooltip"]')
	);
	var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
		return new bootstrap.Tooltip(tooltipTriggerEl);
	});

	// Popover
	var popoverTriggerList = [].slice.call(
		document.querySelectorAll('[data-bs-toggle="popover"]')
	);
	var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
		return new bootstrap.Popover(popoverTriggerEl);
	});

	// SVG Converter
	$("img.svg").each(function () {
		var $img = jQuery(this);
		var imgID = $img.attr("id");
		var imgClass = $img.attr("class");
		var imgURL = $img.attr("src");
		jQuery.get(
			imgURL,
			function (data) {
				var $svg = jQuery(data).find("svg");
				if (typeof imgID !== "undefined") {
					$svg = $svg.attr("id", imgID);
				}
				if (typeof imgClass !== "undefined") {
					$svg = $svg
						.attr("class", imgClass + " replaced-svg")
						.css("display", "inline-block");
				}
				$svg = $svg.removeAttr("xmlns:a");
				$img.replaceWith($svg);
			},
			"xml"
		);
	});

	// Side Menu
	const btnSideMenu = $("#btnSideMenu");
	const sideMenu = $(".side__menu");
	const overlayMenu = $(".overlay__menu");
	if (btnSideMenu.length > 0) {
		btnSideMenu.on("click", () => {
			sideMenu.fadeIn(300);
			overlayMenu.fadeIn(200);
		});
	}
	if (overlayMenu.length > 0) {
		overlayMenu.on("click", () => {
			overlayMenu.fadeOut(300);
			sideMenu.fadeOut(200);
		});
	}

	// Sortable Hobbies
	const sortableHobbies = $("#sortableHobbies");
	if (sortableHobbies.length > 0) {
		sortableHobbies.sortable();
		sortableHobbies.disableSelection();
	}

	// Sortable Languages
	const sortableLanguages = $("#sortableLanguages");
	if (sortableLanguages.length > 0) {
		sortableLanguages.sortable();
		sortableLanguages.disableSelection();
	}

	// Input Tags
	const inputTags = $(".input-tag");
	if (inputTags.length > 0) {
		inputTags.tagsinput("items");
	}

	// Toggle Volume
	const toggleVolume = $(".btn__volume");
	if (toggleVolume.length > 0) {
		toggleVolume.on("click", (e) => {
			e.preventDefault();
			toggleVolume.toggleClass("mute");
		});
	}

	// Toggle Bookmark
	const toggleBookmark = $(".btn__bookmark");
	if (toggleBookmark.length > 0) {
		toggleBookmark.on("click", (e) => {
			e.preventDefault();
			toggleBookmark.toggleClass("active");
		});
	}

	// Toggle Play
	const playRing = $(".btn__play");
	if (playRing.length > 0) {
		playRing.on("click", (e) => {
			e.preventDefault();
			playRing.toggleClass("active");
			if (playRing.hasClass("active")) {
				$(".icon__pause").css("opacity", "1");
				$(".icon__play").css("opacity", "0");
			} else {
				$(".icon__pause").css("opacity", "0");
				$(".icon__play").css("opacity", "1");
			}
		});
	}

	// Mansory
	const gridMansory = $(".gallery-wrapper");
	if (gridMansory.length > 0) {
		gridMansory.masonry({
			itemSelector: ".grid-item",
			columnWidth: ".grid-sizer",
			percentPosition: true,
			transitionDuration: 0,
		});
		gridMansory.imagesLoaded().progress(function () {
			gridMansory.masonry();
		});
	}

	// Copy link
	const btnCopy = $(".btn__copy");
	if (btnCopy.length > 0) {
		btnCopy.on("click", () => {
			let temp = $("<input>");
			$("body").append(temp);
			temp.val($(".text__copy").text()).select();
			document.execCommand("copy");
			temp.remove();
			$(".btn__copy span").text("Copied Link");
			setTimeout(() => {
				$(".btn__copy span").text("Copy Link");
				$(".popover").hide();
			}, 1000);
		});
	}
});
