<div class="side__menu">
    <div class="content d-flex flex-column justify-content-between">
        <div class="top">
            <div class="logo text-center">
                <a href="<?php echo $site_url; ?>/member/dashboard/">
                    <img src="<?php echo $site_path; ?>/images/logo.png" class="img-fluid" alt="">
                </a>
            </div>
            <div class="profile text-center my-lg-50 my-md-40 my-30">
                <div class="img">
                    <img src="<?php echo $site_path; ?>/images/profile.png" class="img-fluid" alt="">
                </div>
                <div class="name mt-10">
                    <span class="text-secondary d-block">Welcome Back</span>
                    <span class="d-block text-primary fw-bold">Mohamed Ramadan</span>
                </div>
            </div>
            <ul class="list-unstyled main__links">
                <li class="<?php if($current_page === "Dashboard") echo 'active' ?>">
                    <a href="<?php echo $site_url; ?>/member/dashboard/" class="d-flex align-items-center p-15">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/dashboard.svg" class="svg" alt="">
                        </span>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="<?php if($current_page === "Account") echo 'active' ?>">
                    <a href="<?php echo $site_url; ?>/member/account/" class="d-flex align-items-center p-15">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/account.svg" class="svg" alt="">
                        </span>
                        <span>Account Info</span>
                    </a>
                </li>
                <li class="<?php if($current_page === "Profile") echo 'active' ?>">
                    <a href="<?php echo $site_url; ?>/member/profile/" class="d-flex align-items-center p-15">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/profile.svg" class="svg" alt="">
                        </span>
                        <span>Profile Image</span>
                    </a>
                </li>
                <li class="<?php if($current_page === "Money") echo 'active' ?>">
                    <a href="<?php echo $site_url; ?>/member/money/" class="d-flex align-items-center p-15">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/money.svg" class="svg" alt="">
                        </span>
                        <span>Add Money</span>
                    </a>
                </li>
                <li class="<?php if($current_page === "Invoices") echo 'active' ?>">
                    <a href="<?php echo $site_url; ?>/member/Invoices/" class="d-flex align-items-center p-15">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/invoices.svg" class="svg" alt="">
                        </span>
                        <span>Invoices</span>
                    </a>
                </li>
                <li class="<?php if($current_page === "History") echo 'active' ?>">
                    <a href="<?php echo $site_url; ?>/member/history/" class="d-flex align-items-center p-15">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/history.svg" class="svg" alt="">
                        </span>
                        <span>Call History</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="bottom">
            <hr class="hr">
            <ul class="list-unstyled main__links">
                <li class="<?php if($current_page === "Email") echo 'active' ?>">
                    <a href="<?php echo $site_url; ?>/member/email/" class="d-flex align-items-center p-15">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/change-email.svg" class="svg" alt="">
                        </span>
                        <span>Change Email Address</span>
                    </a>
                </li>
                <li class="<?php if($current_page === "Password") echo 'active' ?>">
                    <a href="<?php echo $site_url; ?>/member/password/" class="d-flex align-items-center p-15">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/change-password.svg" class="svg" alt="">
                        </span>
                        <span>Change Password</span>
                    </a>
                </li>
                <li class="<?php if($current_page === "Switch") echo 'active' ?>">
                    <a href="<?php echo $site_url; ?>/member/switchaccount/" class="d-flex align-items-center p-15">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/life-saver.svg" class="svg" alt="">
                        </span>
                        <span>Contact Support</span>
                    </a>
                </li>
                <li class="<?php if($current_page === "Switch") echo 'active' ?>">
                    <a href="<?php echo $site_url; ?>/member/switchaccount/" class="d-flex align-items-center p-15">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/switchAcc.svg" class="svg" alt="">
                        </span>
                        <span>Switch Account</span>
                    </a>
                </li>
                <li class="logout">
                    <a href="" class="d-flex align-items-center p-15">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/logout.svg" alt="">
                        </span>
                        <span>Logout</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>