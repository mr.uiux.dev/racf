<div class="overlay__menu"></div>
<div class="header mt-md-25 mt-15">
    <div class="row align-items-center flex-nowrap">
        <div class="col d-xl-none d-flex side__menu__left">
            <button id="btnSideMenu" class="btn__side__menu">
                <img src="<?php echo $site_path ?>/images/icons/menu.svg" alt="">
            </button>
        </div>
        <div class="col">
            <div class="search d-md-block d-none">
                <form action="<?php echo $site_url ?>/cyberfriend/search/">
                    <input type="search" placeholder="Search any topic, person" name="" class="form-control" id="">
                    <button type="submit" class="btn__custom">
                        <img src="<?php echo $site_path ?>/images/icons/search.svg" alt="">
                    </button>
                </form>
            </div>
        </div>
        <div class="col">
            <div class="d-flex justify-content-end align-items-center">
                <!-- disabled -->
                <div class="d-flex align-items-center">
                    <button class="p-0 border-0 bg-transparent" data-bs-container="body" data-bs-toggle="popover"
                        data-bs-placement="top" data-bs-content="Here Text">
                        <img src="<?php echo $site_path ?>/images/icons/question.svg" class="svg questionmark__icon"
                            alt="">
                    </button>
                    <a href="<?php echo $site_url ?>/cyberfriend/acceptcalls/"
                        class="btn__custom btn__green me-15 d-lg-inline-block d-none">
                        Accept Calls
                    </a>
                </div>
                <!-- disabled -->
                <a href="<?php echo $site_url ?>/cyberfriend/acceptcalls/"
                    class="btn__sm__icon btn__sm__video d-lg-none d-flex align-items-center justify-content-center">
                    <img src="<?php echo $site_path ?>/images/icons/video.svg" class="svg" alt="">
                </a>
                <a href="<?php echo $site_url ?>/cyberfriend/search/"
                    class="btn__sm__icon d-md-none d-flex align-items-center justify-content-center">
                    <img src="<?php echo $site_path ?>/images/icons/search.svg" class="svg" alt="">
                </a>
                <div class="dropdown">
                    <button class="btn__custom dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown"
                        aria-expanded="false">
                        <div class="d-flex flex-column">
                            <span class="name">Mohamed Ramadan</span>
                            <span class="price text-primary">$2,200</span>
                        </div>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                        <li><a class="dropdown-item" href="#">Dashboard</a></li>
                        <li><a class="dropdown-item" href="#">How it works</a></li>
                        <li>
                            <hr>
                        </li>
                        <li><a class="dropdown-item" href="#">Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>