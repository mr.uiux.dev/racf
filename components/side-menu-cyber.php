<div class="side__menu">
    <div class="content d-flex flex-column justify-content-between">
        <div class="top">
            <div class="logo text-center">
                <a href="<?php echo $site_url; ?>/cyberfriend/dashboard/">
                    <img src="<?php echo $site_path; ?>/images/logo.png" class="img-fluid" alt="">
                </a>
            </div>
            <div class="profile text-center my-lg-50 my-md-40 my-30">
                <div class="img">
                    <img src="<?php echo $site_path; ?>/images/profile.png" class="img-fluid" alt="">
                </div>
                <div class="name mt-10">
                    <span class="text-secondary d-block">Welcome Back</span>
                    <span class="d-block text-primary fw-bold">Mohamed Ramadan</span>
                </div>
            </div>
            <ul class="list-unstyled main__links">
                <li class="<?php if($current_page === "Dashboard") echo 'active' ?>">
                    <a href="<?php echo $site_url; ?>/cyberfriend/dashboard/" class="d-flex align-items-center p-15">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/dashboard.svg" class="svg" alt="">
                        </span>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="<?php if($current_page === "Calls") echo 'active' ?>">
                    <a href="<?php echo $site_url; ?>/cyberfriend/acceptcalls/" class="d-flex align-items-center p-15">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/video.svg" class="svg" alt="">
                        </span>
                        <span>Accept Calls</span>
                    </a>
                </li>
                <li class="<?php if($current_page === "Cyber Profile") echo 'active' ?>">
                    <a href="<?php echo $site_url; ?>/cyberfriend/cyberprofile/" class="d-flex align-items-center p-15">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/bookmark.svg" class="svg" alt="">
                        </span>
                        <span>Profile Page</span>
                    </a>
                </li>
                <li class="<?php if($current_page === "Account") echo 'active' ?>">
                    <!-- completed, not__completed -->
                    <a href="<?php echo $site_url; ?>/cyberfriend/account/"
                        class="d-flex align-items-center p-15 completed">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/account.svg" class="svg" alt="">
                        </span>
                        <span>Account Info</span>
                    </a>
                </li>
                <li class="<?php if($current_page === "About") echo 'active' ?>">
                    <!-- completed, not__completed -->
                    <a href="<?php echo $site_url; ?>/cyberfriend/about/"
                        class="d-flex align-items-center p-15 completed">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/about.svg" class="svg" alt="">
                        </span>
                        <span>About Me</span>
                    </a>
                </li>
                <li class="<?php if($current_page === "Profile") echo 'active' ?>">
                    <!-- completed, not__completed -->
                    <a href="<?php echo $site_url; ?>/cyberfriend/profile/"
                        class="d-flex align-items-center p-15 completed">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/profile.svg" class="svg" alt="">
                        </span>
                        <span>Profile Image</span>
                    </a>
                </li>
                <li class="<?php if($current_page === "Gallery") echo 'active' ?>">
                    <!-- completed, not__completed -->
                    <a href="<?php echo $site_url; ?>/cyberfriend/gallery/"
                        class="d-flex align-items-center p-15 not__completed">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/gallery.svg" class="svg" alt="">
                        </span>
                        <span>Photo Gallery</span>
                    </a>
                </li>
                <li class="<?php if($current_page === "Hobbies") echo 'active' ?>">
                    <!-- completed, not__completed -->
                    <a href="<?php echo $site_url; ?>/cyberfriend/hobbies/"
                        class="d-flex align-items-center p-15 not__completed">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/hobbies.svg" class="svg" alt="">
                        </span>
                        <span>Hobbies</span>
                    </a>
                </li>
                <li class="<?php if($current_page === "Languages") echo 'active' ?>">
                    <a href="<?php echo $site_url; ?>/cyberfriend/languages/"
                        class="d-flex align-items-center p-15 not__completed">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/languages.svg" class="svg" alt="">
                        </span>
                        <span>Languages Spoken</span>
                    </a>
                </li>
                <li class="<?php if($current_page === "Social") echo 'active' ?>">
                    <!-- completed, not__completed -->
                    <a href="<?php echo $site_url; ?>/cyberfriend/social/"
                        class="d-flex align-items-center p-15 not__completed">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/social.svg" class="svg" alt="">
                        </span>
                        <span>Social Links</span>
                    </a>
                </li>
                <hr class="hr">
                <li class="<?php if($current_page === "Money") echo 'active' ?>">
                    <a href="<?php echo $site_url; ?>/cyberfriend/money/" class="d-flex align-items-center p-15">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/money.svg" class="svg" alt="">
                        </span>
                        <span>Withdraw Money</span>
                    </a>
                </li>
                <li class="<?php if($current_page === "History") echo 'active' ?>">
                    <a href="<?php echo $site_url; ?>/cyberfriend/history/" class="d-flex align-items-center p-15">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/history.svg" class="svg" alt="">
                        </span>
                        <span>Call History</span>
                    </a>
                </li>
                <li class="<?php if($current_page === "Block") echo 'active' ?>">
                    <a href="<?php echo $site_url; ?>/cyberfriend/block/" class="d-flex align-items-center p-15">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/block.svg" class="svg" alt="">
                        </span>
                        <span>Block Users</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="bottom">
            <hr class="hr">
            <ul class="list-unstyled main__links">
                <li class="<?php if($current_page === "Email") echo 'active' ?>">
                    <a href="<?php echo $site_url; ?>/cyberfriend/email/" class="d-flex align-items-center p-15">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/change-email.svg" class="svg" alt="">
                        </span>
                        <span>Change Email Address</span>
                    </a>
                </li>
                <li class="<?php if($current_page === "Password") echo 'active' ?>">
                    <a href="<?php echo $site_url; ?>/cyberfriend/password/" class="d-flex align-items-center p-15">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/change-password.svg" class="svg" alt="">
                        </span>
                        <span>Change Password</span>
                    </a>
                </li>
                <li class="<?php if($current_page === "Switch") echo 'active' ?>">
                    <a href="<?php echo $site_url; ?>/cyberfriend/switchaccount/"
                        class="d-flex align-items-center p-15">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/life-saver.svg" class="svg" alt="">
                        </span>
                        <span>Contact Support</span>
                    </a>
                </li>
                <li class="<?php if($current_page === "Switch") echo 'active' ?>">
                    <a href="<?php echo $site_url; ?>/cyberfriend/switchaccount/"
                        class="d-flex align-items-center p-15">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/switchAcc.svg" class="svg" alt="">
                        </span>
                        <span>Switch Account</span>
                    </a>
                </li>
                <li class="logout">
                    <a href="" class="d-flex align-items-center p-15">
                        <span class="icon me-10">
                            <img src="<?php echo $site_path; ?>/images/icons/logout.svg" alt="">
                        </span>
                        <span>Logout</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>